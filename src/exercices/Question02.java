package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Question02 {

    public static void main(String[] args) {
          
        System.out.println("Liste des femmes du club : \n"); 
       trierLesPersonnesParNomPrenom(listeDesPersonnes);
        
        for ( Personne pers : donnees.Club.listeDesPersonnes){
              
           if ( pers.sexe.equals("F") ) {
           
              System.out.printf("%-15s %-20s \n",
                                
                                pers.nom,
                                pers.prenom
                                
                               );
           
           } 
            
        }  
          
        System.out.println("\n");  
    }      
              
}