package exercices;

import donnees.Personne;
import static utilitaires.UtilDate.ageEnAnnees;

public class Question09 {

  public static void main(String[] args) {
     int compt=0;
     float moyenne=0;
     int age=0;
     String categorie="";
       
         for ( Personne pers : donnees.Club.listeDesPersonnes){
              age=ageEnAnnees(pers.dateNaiss);
         categorie=utilitaires.UtilDojo.determineCategorie( pers.sexe, pers.poids );
           if(categorie=="légers"){    
       
       
                 compt++;
              moyenne=moyenne+age;
             }
      
    }
         moyenne=moyenne/compt;
         
        System.out.printf("Moyenne d'age des membres du club de catégorie légers : %4.1f ans  ",moyenne);
    
   }
}





