package exercices;
import donnees.Personne;
public class Question06 {

    public static void main(String[] args) {
       float pmin=999;
 
   for ( Personne pers : donnees.Club.listeDesPersonnes){
       if(pers.sexe.equals("M") && pers.poids<pmin){
           pmin=pers.poids;
       }
         
       }
        System.out.printf("Poids le plus faible parmi les judoka hommes : %2.0fkg\n",pmin);

  

    }
}

