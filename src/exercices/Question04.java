package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;


public class Question04 {

    
    public static void main(String[] args) {
         
        int cpt=0, cptM=0, cptF=0;   
        
   System.out.println("Effectif du club :\n");    
        
   for ( Personne pers : listeDesPersonnes ){
        cpt ++;
        
        if( pers.sexe.equals("M")){cptM ++;}
        
        if( pers.sexe.equals("F")){cptF ++;}
        
   }
      
      System.out.println("Effectif masculin : "+ cptM);
      System.out.println("Effectif féminin : "+ cptF+ "\n");
      System.out.println("Effectif total : "+ cpt);
   
   System.out.println("\n\n");
        
    }
}

