package exercices;

import donnees.Personne;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDate.dateVersChaine;
import static utilitaires.UtilDate.aujourdhuiChaine;
public class Question08 {

  public static void main(String[] args) {
          int age=0;
          String categorie="";
             System.out.println("Listes des membres masculins de la categorie légers agés de 20 ans ou plus au :"+aujourdhuiChaine());
   for ( Personne pers : donnees.Club.listeDesPersonnes){
       age=ageEnAnnees(pers.dateNaiss);
       categorie=utilitaires.UtilDojo.determineCategorie( pers.sexe, pers.poids );
            
       if(pers.sexe.equals("M") && age>20 && categorie=="légers"){
           
            System.out.printf("%-10s %-10s %-11s %-3sans %-3skg %-15s \n",pers.prenom,pers.nom,dateVersChaine(pers.dateNaiss),ageEnAnnees(pers.dateNaiss),pers.poids,pers.ville);
       }
         
       }
       

   
   }
}




