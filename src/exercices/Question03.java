package exercices;

import donnees.Personne;

public class Question03 {

    public static void main(String[] args) {
                System.out.println("Liste des membres masculin du club pensant plus de 80kg\n");
          for ( Personne pers : donnees.Club.listeDesPersonnes){
    
         if ( pers.sexe.equals("M") && pers.poids>80) {
           
              System.out.printf("%-15s %-20s %3d kg %-10s\n",
                                pers.prenom,
                                pers.nom,
                                pers.poids,
                                pers.ville
                               );
      
    }
}
    }
}
