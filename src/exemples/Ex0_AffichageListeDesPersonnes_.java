package exemples;

//import static donnees.Club.listeDesPersonnes;
import donnees.Personne;


public class Ex0_AffichageListeDesPersonnes_ {

 public static void main(String[] args) {
       
  System.out.println("\nNous sommes le: "+utilitaires.UtilDate.aujourdhuiChaine()+"\n");   
        
  // BOUCLE FOR EACH ( pour chaque) de JAVA  POUR PARCOURIR UNE COLLECTION    
  // pour chaque élément de type Personne que l'on désigne par la variable  pers 
  // et se touvant dans dans la collection  listeDesPersonnes
  // On effectue avec la variable pers le traitement codé entre les deux accolades {   }
  
  // le type des éléments se trouvant dans la collection à parcourir : Personne
  // la variable de parcours: pers
  // le signe : dans l'instruction signifie "se trouvant dans"
  // listeDesPersonnes: la collection à parcourir
  
  for ( Personne pers : donnees.Club.listeDesPersonnes){ // accolade ouvrante marquant le début du traitement
        
     // Traitement à effectuer à chaque tour de boucle 
        
     // Afficher l'élément désigné par pers ( à chaque tour de boucle) 
     
      System.out.println(pers);
      
     // mais cet affichage ne sera pas satisfaisant !  
     // il faut accèder aux divers champs individuellement  voir Ex1 
  
   } // accolade marquant la fin du traitement 
     
  
  
  System.out.println("\n\n");  // on passe deux lignes  
 
  // Si on essaie d'afficher directement la liste sans utiliser de boucle  
  // ce n'est pas satisfaisant non plus: il faut une boucle !

  System.out.println(donnees.Club.listeDesPersonnes);
 
  
  System.out.println("\n\n");  // on passe deux lignes  
 }      
}

   
