package exemples;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
//import static utilitaires.UtilDate.ageEnAnnees;

public class Ex4_AffichageListeDesPersonnesAvecAge {

    public static void main(String[] args) {
        
        System.out.println("\n\n");  
        
        for ( Personne pers : listeDesPersonnes){
        
            int age= utilitaires.UtilDate.ageEnAnnees(pers.dateNaiss);
       
            System.out.printf("%-20s %2d ans\n",pers.nom, age);
        }
          
        System.out.println("\n\n");  
  }      
}

   
