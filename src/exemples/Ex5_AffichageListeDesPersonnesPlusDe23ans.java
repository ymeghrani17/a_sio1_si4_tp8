package exemples;

//import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilDate.ageEnAnnees;

public class Ex5_AffichageListeDesPersonnesPlusDe23ans {

    public static void main(String[] args) {
      
     System.out.println("\n\n");     
        
     for ( Personne pers : donnees.Club.listeDesPersonnes ) {
        
        // On utilise la fonction ageEnAnnees se trouvant dans utilitaires.UtilDate ( import ligne 5)
        // pour calculer l'age correspondant à une date de naissance 
        // on passe le paramètre pers.dateNaiss de type Date
         
        int age= ageEnAnnees(pers.dateNaiss);

        if ( age > 23 ){ 
            
          System.out.printf("%-20s %2d ans\n",pers.nom, age);
          
          // la deuxième règle  %2d  indique un entier sur deux positions  
          
        }
     }
     
     System.out.println("\n\n");  
  }      
}

   
