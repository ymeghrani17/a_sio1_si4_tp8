package exemples;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;

public class Ex1_AffichageListeDesPersonnes {

 public static void main(String[] args) {
         
   // utilisation de le fonction aujourdhuichaine pour afficher la date du jour  
   System.out.println("\nNous sommes le: "+ utilitaires.UtilDate.aujourdhuiChaine()+"\n");    
        
   for ( Personne pers : listeDesPersonnes ){
        
      // il faut traiter les champs de la variable pers individuellement 
      // en utilisant la notation pointée pers.nom 
      // ici on ne récupère que le nom pour l'afficher  
      
      System.out.println(pers.nom);
        
   }
        
   System.out.println("\n\n");  
 }      
}

   
