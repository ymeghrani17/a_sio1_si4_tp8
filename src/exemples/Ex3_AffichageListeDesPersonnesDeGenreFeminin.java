package exemples;

//import static donnees.Club.listeDesPersonnes;
import donnees.Personne;

public class Ex3_AffichageListeDesPersonnesDeGenreFeminin {

    public static void main(String[] args) {
        
        System.out.println("\n\n");  
        
        // BOUCLE DE PARCOURS FOR EACH cf EX0
        
        for ( Personne pers : donnees.Club.listeDesPersonnes){
           
           //  Ici le traitement appliqué   à la variable pers est conditionnel  
           //  Attention la comparaison de chaîne de caractères doit se faire avec
           //  .equals comme cela apparait ci-dessous
           //  le test doit toujours se mettre entre parenthèses   (  )
              
           if ( pers.sexe.equals("F") ) {
           
              System.out.printf("%-15s %-20s %3d Victoires\n",
                                pers.prenom,
                                pers.nom,
                                pers.nbVictoires
                               );
           
           } // accolade fermante de l'instruction if 
            
        }  // accolade fermante de la boucle for
          
        System.out.println("\n\n");  
    }      
}