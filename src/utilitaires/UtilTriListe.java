package utilitaires;

import donnees.Personne;
import static java.util.Collections.*;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class UtilTriListe {
    
    public static void trierLesPersonnesParNomPrenom(List<Personne> lp){
          sort(lp, new ComparateurPersonnesParNomPrenom());
    }
    public static  void trierLesPersonnesParSexeNomPrenom(List<Personne> lp){
          sort(lp, new ComparateurPersonnesParSexeNomPrenom());
    }
    public static  void trierLesPersonnesParPoids(List<Personne> lp){
          sort(lp, new ComparateurPersonnesParPoids());
    }
    public static  void trierLesPersonnesParNom(List<Personne> lp){
          sort(lp, new ComparateurPersonnesParNom());
    }
    public static  void trierLesPersonnesParNbVictoiresDecroissant(List<Personne> lp){
          
          sort(lp, new ComparateurPersonnesParNbVictoiresDecroissant());
          reverse(lp);
    }  

    //////////////////////////////////////           COMPARATEURS                 /////////////////////////////////////////////////////////////////////////////////
    
    static class  ComparateurPersonnesParNbVictoiresDecroissant implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Personne p  = (Personne)   t ;
          Personne p1 = (Personne)  t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nb de victoires">
        
           if       ( p.nbVictoires  > p1.nbVictoires)   resultat =   1;
           else if  ( p.nbVictoires  < p1.nbVictoires )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
    }    
}
    static class ComparateurPersonnesParNom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0; 
        
         Personne p  = (Personne)  t ;
         Personne p1 = (Personne)  t1;
            
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom">
            
            resultat=  p.nom.compareTo(p1.nom);
            
            //</editor-fold>
        
       
        
        return resultat;  
    }    
}    
    static class ComparateurPersonnesParNomPrenom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Personne p  = (Personne)  t ;
        Personne p1 = (Personne)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par nom/prénom">
            
            resultat=  p.nom.compareTo(p1.nom);
            if (resultat==0) resultat= p.prenom.compareTo(p1.prenom);
            
        //</editor-fold>
         
        return resultat;  
    }    
}  
    static class ComparateurPersonnesParSexeNomPrenom implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
        int resultat=0;
       
        Personne p  = (Personne)  t ;
        Personne p1 = (Personne)  t1;
            
        //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par sexe/nom/prénom">
           
        resultat= p.sexe.compareTo(p1.sexe);
        if ( resultat==0){
             resultat=  p.nom.compareTo(p1.nom);
             if (resultat==0) resultat= p.prenom.compareTo(p1.prenom);
         }            
        
        //</editor-fold>
      
        return resultat;  
    }    
} 
    static class ComparateurPersonnesParPoids implements Comparator{

    @Override
    public int compare(Object t, Object t1) {
        
         int resultat=0;
      
          Personne p  =  (Personne)   t ;
          Personne p1 = (Personne)  t1;
          
            //<editor-fold defaultstate="collapsed" desc="CODE SPECIFIQUE DU COMPARATEUR par poids">
        
           if        ( p.poids   > p1.poids )   resultat =   1;
           else if  ( p.poids   < p1.poids )   resultat =  -1; 
            
            //</editor-fold>
        
            return resultat;  
    }    
}

}


